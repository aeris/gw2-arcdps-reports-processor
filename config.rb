ME = 'aeris.5846'
LANG = :fr

IDS = {
  amam: 17021, siaxx: 17028, ensolyss: 16948,

  skorvald: 17632, artsariiv: 17949, arkk: 17759,

  ai: 23254, elemental_ai: 23254, dark_ai: 23254,

  vale_guardian: 15438, gorseval: 15429, sabetha: 15375,

  slothasor: 16123, trio: 16088, matthias: 16115,

  keep_construct: 16235, twisted_castle: 16247, xera: 16246,

  cairn: 17194, mursaat: 17172, samarog: 17188, deimos: 17154,

  soulless_horror: 19767, dhuum: 19450,

  conjured_amalgamate: 43974,

  adina: 22006, sabir: 21964,

  icebrood: 22154, kodans: 22481, fraenir: 22492, boneskinner: 22521,
  whisper_jormag: 22711, freezie: 21333,

  standard_golem: 16199, medium_golem: 19645, large_golem: 19676
}.freeze

BOSSES = {
  fractals: {
    '98cm': %i[amam siaxx ensolyss],
    '99cm': %i[skorvald artsariiv arkk],
    '100cm': %i[elemental_ai dark_ai]
  },
  raids: {
    spirit_vale: %i[vale_guardian gorseval sabetha],
    salvation_pass: %i[slothasor trio matthias],
    stronghold_faithfull: %i[keep_construct twisted_castle xera],
    bastion_penitent: %i[cairn mursaat samarog deimos],
    hall_chains: %i[soulless_horror river_souls statues_grenth dhuum],
    mythwright_gambit: %i[conjured_amalgamate twin_largos qadim],
    key_ahdashim: %i[adina sabir qadim_peerless]
  },
  strikes: %i[icebrood kodans fraenir boneskinner whisper_jormag forging_steel cold_war freezie],
  golems: %i[standard_golem medium_golem large_golem]
}.freeze

ARCDPS_BOSSES = {
  fractals: %i[ai arkk arts drkai elai ensol mama siax skorv],
  raids: %i[adina ca cairn dei dhuum gors kc matt mo sab sabir sam sh sloth trio twstcstl vg xera],
  strikes: %i[boneskin fraenir freezie icebrood supkodbros woj],
  golems: %i[LGolem MedGolem StdGolem]
}.freeze

I18N = {
  fr: {
    raids: 'Raids',
    fractals: 'Fractales des Brumes',
    strikes: 'Missons d’attaque',
    golems: 'Golems',

    '98cm': 'Cauchemars',
    '99cm': 'Observatoire détruit',
    '100cm': 'Pic de Sunqua',

    spirit_vale: 'Vallée des esprits',
    salvation_pass: 'Passage de la rédemption',
    stronghold_faithfull: 'Forteresse des fidèles',
    bastion_penitent: 'Bastion du pénitent',
    hall_chains: 'Hall des chaînes',
    mythwright_gambit: 'Gambit de Forgeconte',
    key_ahdashim: 'Clé d’Ahdashim',

    amam: 'AMAM',
    siaxx: 'Siaxx',
    ensolyss: 'Ensolyss',

    skorvald: 'Skorvald',
    artsariiv: 'Artsariiv',
    arkk: 'Arkk',

    ai: 'Ai, Keeper of the Peak',
    elemental_ai: 'Elemental Ai, Keeper of the Peak',
    dark_ai: 'Dark Ai, Keeper of the Peak',

    vale_guardian: 'Gardien de la vallée',
    gorseval: 'Gorseval le disparate',
    sabetha: 'Sabetha la saboteuse',

    slothasor: 'Paressor',
    trio: 'Trio de bandits',
    matthias: 'Matthias Gabrel',

    keep_construct: 'Titan du fort',
    twisted_castle: 'Chateau corrompu',
    xera: 'Xera',

    cairn: 'Cairn',
    mursaat: 'Ssurveillant mursaat',
    samarog: 'Samarog',
    deimos: 'Deimos',

    soulless_horror: 'Horreur sans âme',
    dhuum: 'Dhuum',

    conjured_amalgamate: 'Amalgame conjurée',

    adina: 'Cardinale Adina',
    sabir: 'Cardinal Sabir',

    icebrood: 'Sentinelle couvregivre',
    kodans: 'Kodans',
    fraenir: 'Fraenir de Jormag',
    boneskinner: 'Dessosseur',
    whisper_jormag: 'Murmure de Jormag',
    freezie: 'Frisquet',

    standard_golem: 'Golem chat standard',
    medium_golem: 'Golem chat intermédiaire',
    large_golem: 'Grand chat golem',
  }
}.freeze.fetch LANG
