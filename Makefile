.DEFAULT_GOAL := all

process:
	./process
.PHONY: process

rsync:
	rsync --progress -ahxvAHX --delete html/ kamino:/srv/www/fr.imirhil/www/gw2/

all:
	gw2-post-exit
	$(MAKE) process
	$(MAKE) rsync

html/static.html: static.html.erb
	erb "$<" > "$@"
static: html/static.html
	$(MAKE) rsync
