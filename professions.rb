#!/usr/bin/env ruby
require 'amazing_print'
require 'date'
require 'zlib'
require 'oj'
require_relative 'config.rb'

reports = ARCDPS_BOSSES.fetch(:raids).collect do |boss|
    Dir["html/*_#{boss}_*.json.gz"].collect do |file|
        name = File.basename file
        date, boss, *_ = name.split '_'
        date = date.sub '-', ''
        date = DateTime.parse date
        [file, date, boss]
    end.group_by { |_, date, boss| [date.to_date, boss] }
    .to_h
    .transform_values { |v| v.sort { |a, b| a[1] <=> b[1] }.first }
    .collect { |_, v| v.first }
end.flatten(1)

count = reports.count
puts "#{count} reports"

reports = reports.collect do |file|
    json = File.open file do |fid|
        json = Zlib::GzipReader.new fid
        Oj.load json.read
    end
    json.fetch('players').collect { |p| p.fetch('profession') }
end.flatten(1)

count = reports.count
reports = reports.inject(Hash.new 0) { |h, e| h[e] += 1; h }
                    .sort { |a, b| b.last <=> a.last }
                    .to_h
                    .transform_values { |c| "#{c} (#{(c.to_f/count*100).round(2)}%)" }

ap reports
